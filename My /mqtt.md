# MQTT (Message Queuing Telemetry Transport) : 

* A simple messaging protocol designed for constrained devices with low bandwidth.
* A lightweight publish and subscribe system to publish and receive messages as a client.
* A perfect solution for IOT applications, also allows to send commands to control outputs, read and publish data from sensor nodes.
* Communication between several devices can be established simultaneously.

**A general overview of how communication takes place within MQTT devices**. 

![Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol](uploads/358848b4b9222917e31db143ff0a6383/Main-entities-of-the-Message-Queuing-Telemetry-Transport-MQTT-protocol.png)

MQTT Client as a **publisher** sends a message to the **MQTT broker** whose work is to distribute the message accordingly to all other MQTT clients **subscribed** to the topic on which publisher publishes the message.

**Topics** are a way to register interest for incoming messages or to specify where to publish the message. Represented by strings, separated by forward slash. Each slash indicates a topic level.


## MQTT configuration in Shunya  

| Config | Description |
|-|-|
| MQTT broker url | MQTT broker url |
| Username | MQTT broker username for security (optional)  |
| Password | MQTT broker password for security (optional) |
| Client ID | Unique Identification of the client, user can put any name that is convenient. |

The configurations need to be set manually by the user before using the API's

For example: 

A sample JSON snippet should contain
```json
"mqtt": {
    "brokerUrl": "",
    "userName": "",
    "password": "",
    "clientID": "",
}
``` 


## Commonly configured parameters


For example: 

A typical mqtt configuration without security will look like this  

```json
"mqtt": {
    "brokerUrl": "192.168.0.1:8080",
    "clientID": "shunya pi3",
}
``` 


## MQTT API
| **API** | **Description** | **Details** |
|---------|-----------------|-------------|
| `newMqtt()` | Creates a new MQTT Instance | [Read More](#newmqtt) |
| `mqttConnect()`  | Connects to given MQTT broker in the settings | [Read More](#mqttconnect) |
| `mqttPublish()` | Publishes the data to the MQTT broker | [Read More](#mqttpublish) |
| `mqttSubscribe()` | Subscribes to the given topic and callback can be used to run a user-defined function when the devices recieves a message from the topic | [Read More](#mqttsubscribe) |
| `mqttDisconnect()`  | Disconnects the MQTT Broker | [Read More](#mqttdisconnect) |
---

## MQTT API

### `newMqtt()`

**Description** : Creates a new MQTT Instance

**Parameters**

* `name (char *)` - Name of the JSON object.

**Return-type** : mqttObj

**Returns** : mqttObj Instance for MQTT

**Usage** : 

For example: Lets say your JSON file looks like

```json
"mqttbroker1": {
    "brokerUrl": "192.168.0.1:8080",
    "clientID": "shunya pi3",
}
``` 

So the usage of the API's will be 
```c
mqttObj broker1 = newMqtt("mqttbroker1");
```

---

### `mqttConnect()`

**Description**: Connects to given MQTT broker with details given in the settings

**Parameters**

* `*obj`(mqttObj) - Pointer to the MQTT Instance.

**Return-type** : void

**Usage** : 
```c
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;
```
---

### `mqttPublish()`

**Description**: Publishes the data to the MQTT broker

**Parameters**

* `*obj`(mqttObj) - Pointer to the MQTT Instance.
* `*topic(char)` -  Topic the Message/data needs to be sent.
* `*fmt(const char)` - Message/data that needs to be sent.

**Return-type** : void

**Usage** : 
```c
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;
/* Publish to topic */
mqttPublish(&broker1, "topic","message %d", number) ;
```
---

### `mqttSubscribe()`

**Description**: Subscribes to the given topic and callback can be used to run a user-defined function when the devices receive a message from the topic
**Parameters**

* `*obj`(mqttObj) - Pointer to the MQTT Instance.
* `*topic(char)` -  Topic the Message/data needs to be sent.
* `void (*callback)(int, char *, int, char *)` - The pointer to the callback function

**Return-type** : void

**Usage** : 

```c
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");
/* Connect to broker */
mqttConnect(&broker1) ;
/* Subscribe to topic */
mqttSubscribe(&broker1, "test1") ;
```

---

### `mqttSetSubCallback()`

**Description**:  Set the subscribe callback function that executes the when the topic subscribed gets a message.

The Callback function arguments need to be in the specified format,
```c
void callback(int topicLen, char *topic, int msgLen, char *message)
```

This format is used to pass the message received to the callback function.

**Parameters**

* `*obj`(mqttObj) - Pointer to the MQTT Instance.
* `callback (void *)` - A callback function that will get called when the subscribed topic gets a message

**Return-type** : void

**Usage** : 

```c
/* Create New Instance */
mqttObj broker1 = newMqtt("mqttbroker1");

void callback(int topicLen, char *topic, int msgLen, char *message)
{
    printf("Received Message is: %s", message);
}

mqttSetSubCallback(&broker1, callback);
```
---

### `mqttDisconnect()`

**Description**: Disconnects the MQTT Broker

**Parameters**:

* `*obj`(mqttObj) - Pointer to the MQTT Instance.

**Return-type** : void

**Usage** : 
```c
mqttDisconnect(&broker1) ;
```
---