# SMS Alerts 

The SMS alerts are sent using Twilio online SMS service. Internally the Shunya interfaces send HTTP requests to the Twilio cloud to send the Whatsapp or SMS messages.

The SMS alerts can be sent using 
1. Twilio
2. SMSGupShup (API's Comming soon)


## Twilio configuration in Shunya  

| Config | Description |
|-|-|
| Account SID | Account SID from the Twilio console  |
| Auth Token | Authorization Token from the Twilio console. |

The configurations need to be set manually by the user before using the API's

For example: 

A sample JSON snippet should contain
```json
"twilio": {
    "account sid": "",
    "auth token": "",
},
``` 


## Commonly configured parameters


For example: 

A typical Twilio configuration will look like this  

```json
"twilio": {
    "account sid": "xxxxxxxxxxxxxx",
    "auth token": "xxxxxxxxxxxxxxx",
},
``` 

## API
| **API** | **Description** | **Details** |
| ------ | ------ | ------ |
| `newTwilio()` | Create new Twilio Instance. | [Read More](#newTwilio) 
| `sendWhatsappTwilio()` | Send Whatsapp Message via Twilio API. | [Read More](#sendwhatsapptwilio) |

### `newTwilio()`

**Description** : Creates a new Twilio Instance

**Parameters**

* `name (char *)` - Name of the JSON object.

**Return-type** : twilioObj

**Returns** : twilioObj Instance for Twilio

**Usage** : 

For example: Lets say your JSON file looks like

```json
"twilio1": {
    "account sid": "xxxxxxxxxxxxxx",
    "auth token": "xxxxxxxxxxxxxxx",
},
``` 

So the usage of the API's will be 
```c
twilioObj whatsapp = newTwilio("twilio1");
```

---

### `sendSmsTwilio()`

**Description**: Send SMS Message via Twilio API.
  

**Parameters**  

*  `obj`(twilioObj) - the Twilio Instance Object.
*  `from_number`(int) - the sending (Number in your Twilio account).
*  `to_number`(int) - the Receiving mobile number.
*  `msg`(char *) - the msg to be sent.

**Return-type** : int8_t

**Returns**: 0 on Success and -1 on Failure. 

**Usage** : 
```c
twilioObj whatsapp = newTwilio("twilio1");
sendWhatsappTwilio(whatsapp,"+911765765789","+911475289631",
                     "Temperature is above %d deg C", temp); 
```
---