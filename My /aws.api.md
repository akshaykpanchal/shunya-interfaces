## AWS IoT core 

AWS IoT core is cloud service provided by Amazon which allows you to communicate and manage
IOT edge devices.

Shunya Interfaces implements communication with the AWS cloud using MQTT protocol.


## AWS configuration in Shunya  

| Config | Description |
|-|-|
| Endpoint | AWS endpoint url |
| Port | AWS connection port, usually it is 443, 8883 incase 443 is blocked by company proxy |
| Certificate dir | Directory where we store the AWS certificates, default for shunya is /home/shunya/.cert/aws |
| Root CA certificate name | AWS provides you with the Root CA certificate. Used for secure communication with AWS. |
| Client certificate | AWS provides certificates that is unique to the each IoT device. Used for secure communication with AWS. |
| Client Key | AWS provides a key that are unique to the each IoT device. Used for secure communication with AWS. |
| Client ID | Unique Identification of the client, user can put any name that is convenient. |

The configurations need to be set manually by the user before using the API's

For example: 

A sample JSON snippet should contain
```json
"aws": {
    "endpoint": "endpoint-name-here",
    "port": 443,
    "certificate dir": "/path/to/certificates/",
    "root certificate": "name-of-root-CA.crt",
    "client certificate": "name-of-client-certificate.pub",
    "private key": "name-of-private-key.pub",
    "client ID": "unique name given by the user",
},
``` 


## Commonly configured parameters


For example: 

A typical configuration will look like this  

```json
"aws": {
    "endpoint": "awer8394haiouery87324.aws",
    "port": 8883,
    "certificate dir": "/home/shunya/.cert/aws/",
    "root certificate": "rootCA.crt",
    "client certificate": "13987wer.pub",
    "private key": "13987wer.pub",
    "client ID": "shunya pi 3",
},
``` 


## AWS API

| **API** | **Description** | **Details** |
|---------|-----------------|-------------|
| `newAws()` | Creates a new AWS Instance | [Read More](#newaws) |
| `awsConnectMqtt()` | Connects to the AWS IoT Cloud | [Read More](#awsconnectmqtt) |
| `awsPublishMqtt()` | Publishes to the given Topic | [Read More](#awspublishmqtt) |
| `awsSubscribeMqtt()` | Subscribes to the given Topic | [Read More](#awssubscribemqtt) |
| `awsSetSubCallbackMqtt()` | Set subscribe callback function | [Read More](#awssetsubcallbackmqtt) |
| `awsDisconnectMqtt()` | Subscribes to the given Topic | [Read More](#awsdisconnectmqtt) |

---

### `newAws()`

**Description** : Creates a new AWS Instance

**Parameters**

* `name (char *)` - Name of the JSON object.

**Return-type** : awsObj

**Returns** : awsObj Instance for AWS

**Usage** : 

For example: Lets say your JSON file looks like

```json
"awsAccount1": {
    "endpoint": "awer8394haiouery87324.aws",
    "port": 8883,
    "certificate dir": "/home/shunya/.cert/aws/",
    "root certificate": "rootCA.crt",
    "client certificate": "13987wer.pub",
    "private key": "123234wer.pub",
    "client ID": "shunya pi 3",
},
``` 

So the usage of the API's will be 
```c
awsObj awsAccount1 = newAws("awsAccount1");
```

---

### `awsConnectMqtt()`

**Description** : Connects to the AWS IoT Cloud.

**Parameters**

* `*obj`(awsObj) - Pointer to the AWS Instance.

**Return-type** : int32_t

**Returns** : AWS Status Code on ERROR or SUCCESS

**Usage** : 
```c
/* Load settings and then connect to AWS */
awsObj awsAccount1 = newAws("awsAccount1");
awsConnectMqtt(&awsAccount1);
```
---

### `awsPublishMqtt()`

**Description** :  Publishes to the given Topic

**Parameters**

* `*obj`(awsObj) - Pointer to the AWS Instance.
* `topic (char *)` - MQTT Topic to publish.
* `msg (char *)` - Message that needs to be published.

**Return-type** : int32_t

**Returns** : AWS Status Code on ERROR or SUCCESS

**Usage** : 
```c
/* Load settings and then connect to AWS */
awsObj awsAccount1 = newAws("awsAccount1");
awsConnectMqtt(&awsAccount1);
awsPublishMqtt(&awsAccount1, "topic", "Send this message");
```
---


### `awsSubscribeMqtt()`

**Description**:  Subscribes to the given Topic.

**Parameters**

* `*obj`(awsObj) - Pointer to the AWS Instance.
* `topic (char *)` - MQTT Topic to subscribe.


**Return-type** : int32_t

**Returns** : AWS Status Code on ERROR or SUCCESS

**Usage** : 

```c
/* Load settings and then connect to AWS */
awsObj awsAccount1 = newAws("awsAccount1");
awsConnectMqtt(&awsAccount1);
awsSubscribeMqtt(&awsAccount1, "topic");
```

---

### `awsSetSubCallbackMqtt()`

**Description**:  Set the subscribe callback function that executes the when the topic subscribed gets a message.

The Callback function arguments need to be in the specified format,
```c
void callback(int topicLen, char *topic, int msgLen, char *message)
```

This format is used to pass the message received to the callback function.

**Parameters**

* `*obj`(awsObj) - Pointer to the AWS Instance.
* `callback (void *)` - A callback function that will get called when the subscribed topic gets a message

**Return-type** : int32_t

**Returns** : AWS Status Code on ERROR or SUCCESS

**Usage** : 

```c
/* Load settings and then connect to AWS */
awsObj awsAccount1 = newAws("awsAccount1");

void callback(int topicLen, char *topic, int msgLen, char *message)
{
    printf("Received Message is: %s", message);
}

awsSetSubCallbackMqtt(&awsAccount1, callback);
```
---

### `awsDisconnectMqtt()`

**Description**: Disconnects the AWS MQTT Broker.

**Parameters**:

* `*obj`(awsObj) - Pointer to the AWS Instance.

**Return-type** : void

**Usage** : 
```c
awsDisconnectMqtt(&awsAccount1);
```
---