# How to install Shunya-Interfaces

## Pre-requisites
You need Shunya OS installed on your favorite Development device. 

For instructions on "How to install Shunya OS on my favorite device"
1. Goto [Install Shunya OS on my favorite device](http://docs.shunyaos.org/)
2. Select your favorite device.
3. Follow Steps given in the `Flashing Shunya OS` section.    

## Install Shunya-Interfaces using Package Manager
 
Boot into Shunya OS and Run this command 

```shell
$ sudo opkg update
$ sudo opkg install shunya-interfaces
```
Oops!! Getting errors by running the above command? Check out our next section on `Compile & Install Shunya-Interfaces from scratch`

## Compile & Install Shunya-Interfaces from scratch   
Oops!! Getting errors by running the above command ? Then try compiling 
shunya-interfaces using the steps given [here](./dd-compiling-and-installing-shunya-interfaces)