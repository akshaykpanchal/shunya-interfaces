# User Documentation

## Overview
| **Popular Topics** | **Description** |
| ------ | ------ |
|[How to install Shunya-Interfaces](how-to-install-shunya-interfaces.md) | Install shunya-interfaces on Shunya OS |
|[Shunya Interfaces API chart](/user-docs/shunya-interfaces-API.md) | API Documentation |
|[How to use GPIO interface via Shunya-Interfaces](/user-docs/GPIO-interface.md) | Example GPIO code and API  | 
|[How to use I2C interface via Shunya-Interfaces](/user-docs/I2C-interface.md)| Example I2C code and API|
|[How to use SPI interface via Shunya-Interfaces](/user-docs/SPI-interface.md)| Example SPI code and API|
|[How to use UART interface via Shunya-Interfaces](/user-docs/UART-interface.md)| Example UART code and API |
|[How to use Interrupts via Shunya-Interfaces](/user-docs/Interrupts.md)| Example code and API|
|[How to use ThingSpeak via Shunya-Interfaces]()|(Comming Soon..)|
